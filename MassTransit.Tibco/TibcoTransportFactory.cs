﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magnum.Caching;
using MassTransit.Tibco.Configuration.Configurators;
using MassTransit.Transports;
using TIBCO.EMS;
using System.Collections.Generic;
using Magnum.Extensions;
using MassTransit.Exceptions;
using MassTransit.Tibco.Configuration.Builders;

namespace MassTransit.Tibco
{
    public class TibcoTransportFactory : ITransportFactory
    {
        readonly Cache<ConnectionFactory, ConnectionHandler<TibcoConnection>> _connections;
        readonly Cache<ConnectionFactory, IConnectionFactoryBuilder> _connectionFactoryBuilders;
        private bool _disposed;

        public TibcoTransportFactory(IEnumerable<KeyValuePair<Uri, IConnectionFactoryBuilder>> connectionFactoryBuilders)
        {
            _connections = new ConcurrentCache<ConnectionFactory, ConnectionHandler<TibcoConnection>>();
            Dictionary<ConnectionFactory, IConnectionFactoryBuilder> builders = connectionFactoryBuilders
              .Select(x => new KeyValuePair<ConnectionFactory, IConnectionFactoryBuilder>(
                  TibcoEndpointAddress.Parse(x.Key).ConnectionFactory, x.Value))
              .ToDictionary(x => x.Key, x => x.Value);

            _connectionFactoryBuilders = new ConcurrentCache<ConnectionFactory, IConnectionFactoryBuilder>(builders);
        }

        public TibcoTransportFactory()
        {
            _connections = new ConcurrentCache<ConnectionFactory, ConnectionHandler<TibcoConnection>>();
            _connectionFactoryBuilders = new ConcurrentCache<ConnectionFactory, IConnectionFactoryBuilder>();

        }

        public void Dispose()
        {
            Dispose(true);
        }

        void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
                _connections.Each(x => x.Dispose());
            _connections.Clear();

            _disposed = true;
        }


        public IDuplexTransport BuildLoopback(ITransportSettings settings)
        {
            if (_disposed)
                throw new ObjectDisposedException("TibcoTransportFactory");

            TibcoEndpointAddress address = TibcoEndpointAddress.Parse(settings.Address.Uri);
            var transport = new Transport(address, () => BuildInbound(settings), () => BuildOutbound(settings));

            return transport;
        }

        public IInboundTransport BuildInbound(ITransportSettings settings)
        {
            if (_disposed)
                throw new ObjectDisposedException("TibcoTransportFactory");

            TibcoEndpointAddress address = TibcoEndpointAddress.Parse(settings.Address.Uri);
            EnsureProtocolIsCorrect(address.Uri);
            ConnectionHandler<TibcoConnection> connectionHandler = GetConnection(_connections, address);

            return new InboundTibcoTransport(address, connectionHandler, SessionMode.AutoAcknowledge);
        }

        public IOutboundTransport BuildOutbound(ITransportSettings settings)
        {
            if (_disposed)
                throw new ObjectDisposedException("TibcoTransportFactory");

            TibcoEndpointAddress address = TibcoEndpointAddress.Parse(settings.Address.Uri);

            EnsureProtocolIsCorrect(address.Uri);

            ConnectionHandler<TibcoConnection> connectionHandler = GetConnection(_connections, address);

            return new OutboundTibcoTransport(address, connectionHandler, SessionMode.AutoAcknowledge);
        }

        ConnectionHandler<TibcoConnection> GetConnection(
          Cache<ConnectionFactory, ConnectionHandler<TibcoConnection>> cache, ITibcoEndpointAddress address)
        {
            ConnectionFactory factory = SanitizeConnectionFactory(address);

            return cache.Get(factory, _ =>
            {


                IConnectionFactoryBuilder builder = _connectionFactoryBuilders.Get(factory, __ =>
                {
                    var configurator = new ConnectionFactoryConfiguratorImpl(address);
                    return configurator.CreateBuilder();
                });

                ConnectionFactory connectionFactory = builder.Build();


                var connection = new TibcoConnection(connectionFactory.Url, address.Username, address.Password);
                var connectionHandler = new ConnectionHandlerImpl<TibcoConnection>(connection);
                return connectionHandler;
            });
        }

        ConnectionFactory SanitizeConnectionFactory(ITibcoEndpointAddress address)
        {
            ConnectionFactory factory = address.ConnectionFactory;

            foreach (ConnectionFactory builder in _connectionFactoryBuilders.GetAllKeys())
            {
                if (string.Compare(factory.Url, builder.Url, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return builder;
                }
            }

            return address.ConnectionFactory;
        }


        static void EnsureProtocolIsCorrect(Uri address)
        {
            if (address.Scheme != "tcp")
            {
                throw new EndpointException(address,
                    "Address must start with 'tcp' not '{0}'".FormatWith(address.Scheme));
            }
        }



        public IOutboundTransport BuildError(ITransportSettings settings)
        {
            return BuildOutbound(settings);
        }

        public IEndpointAddress GetAddress(Uri uri, bool transactional)
        {
            return TibcoEndpointAddress.Parse(uri);
        }

        public string Scheme
        {
            get
            {
                if (_disposed)
                    throw new ObjectDisposedException("RabbitMQTransportFactory");

                return "tcp";
            }
        }

        public IMessageNameFormatter MessageNameFormatter
        {
            get
            {
                if (_disposed)
                    throw new ObjectDisposedException("RabbitMQTransportFactory");

                return new DefaultMessageNameFormatter("::", "--", ":", "-");
            }
        }
    }


}
