﻿using TIBCO.EMS;
using Connection = MassTransit.Transports.Connection;

namespace MassTransit.Tibco
{
    public class TibcoConnection : Connection
    {
        private readonly string _username;
        private readonly string _pwd;
        private TIBCO.EMS.Connection _connection;

        public TibcoConnection(string serverUrl, string username, string pwd,
            int connectionTimeout = 3000)
        {
            _username = username;
            _pwd = pwd;
            ServerUrl = serverUrl;
            ConnectionTimeout = connectionTimeout;
        }

        public string ServerUrl { get; set; }
        public int ConnectionTimeout { get; set; }

        public void Dispose()
        {
            if (!_connection.IsDisconnected())
            {
                _connection.Stop();
            }

            if (!_connection.IsClosed)
                _connection.Close();

            _connection = null;
        }

        public void Connect()
        {
            if (_connection == null)
            {
                var factory = new ConnectionFactory(ServerUrl);
                factory.SetConnAttemptTimeout(ConnectionTimeout);
                _connection = factory.CreateConnection(_username, _pwd);
            }

            _connection.Start();
        }

        public void Disconnect()
        {
            _connection.Stop();
        }

        public Session CreateSession(SessionMode mode)
        {
            return _connection.CreateSession(false, mode);
        }
    }
}