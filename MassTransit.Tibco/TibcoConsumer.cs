﻿using System.Collections;
using MassTransit.Transports;
using TIBCO.EMS;

namespace MassTransit.Tibco
{
    public class TibcoConsumer : ConnectionBinding<TibcoConnection>
    {
        private readonly ITibcoEndpointAddress _address;
        private readonly object _lock = new object();

        public TibcoConsumer(ITibcoEndpointAddress address, SessionMode mode)
        {
            Mode = mode;
            _address = address;
        }

        public SessionMode Mode { get; set; }
        public Session Session { get; set; }
        public MessageConsumer Consumer { get; set; }
        public Destination DestinationTarget { get; set; }

        public void Bind(TibcoConnection connection)
        {
            Session = connection.CreateSession(Mode);
            var lookup = new LookupContext(new Hashtable
            {
                {LookupContext.PROVIDER_URL, connection.ServerUrl},
                {LookupContext.SECURITY_PRINCIPAL, _address.Username},
                {LookupContext.SECURITY_CREDENTIALS, _address.Password}
            });

            lock (_lock)
            {
                DestinationTarget = lookup.Lookup(_address.Uri.Segments[1]) as Destination;
                Consumer = Session.CreateConsumer(DestinationTarget);
            }
        }

        public void Unbind(TibcoConnection connection)
        {
            lock (_lock)
            {
                Consumer.Close();
                Consumer = null;
                DestinationTarget = null;
                Session.Close();
                Session = null;
                connection.Disconnect();
                connection.Dispose();
            }
        }
    }
}