﻿using System;
using System.Collections;
using System.IO;
using MassTransit.Transports;
using TIBCO.EMS;

namespace MassTransit.Tibco
{
    public class TibcoProducer : ConnectionBinding<TibcoConnection>
    {
        private readonly ITibcoEndpointAddress _address;
        private readonly object _lock = new object();

        public TibcoProducer(ITibcoEndpointAddress address, SessionMode mode)
        {
            _address = address;
            Mode = mode;
        }

        public SessionMode Mode { get; set; }
        public MessageProducer Producer { get; set; }
        public Destination DestinationTarget { get; set; }
        public Session Session { get; set; }

        public void Bind(TibcoConnection connection)
        {
            Session = connection.CreateSession(Mode);
            var lookup = new LookupContext(new Hashtable
            {
                {LookupContext.PROVIDER_URL, connection.ServerUrl},
                {LookupContext.SECURITY_PRINCIPAL, _address.Username},
                {LookupContext.SECURITY_CREDENTIALS, _address.Password}
            });

            lock (_lock)
            {
                DestinationTarget = lookup.Lookup(_address.Uri.Segments[1]) as Destination;
                Producer = Session.CreateProducer(DestinationTarget);
            }
        }

        public void Unbind(TibcoConnection connection)
        {
            lock (_lock)
            {
                Producer.Close();
                Producer = null;
                DestinationTarget = null;
                Session.Close();
                Session = null;
                connection.Disconnect();
                connection.Dispose();
            }
        }

        public void Send(MemoryStream message, string msgid)
        {
            var msg = Session.CreateStreamMessage();
            msg.MessageID = msgid;
            msg.ReadBytes(message.ToArray());
            Producer.Send(DestinationTarget, msg);
        }
    }
}