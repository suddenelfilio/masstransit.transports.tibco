﻿using System;
using MassTransit.EndpointConfigurators;
using MassTransit.Tibco.Configuration.Configurators;

namespace MassTransit.Tibco.Configuration
{
    public static class TibcoServiceBusExtensions
    {

        public static void UseTibco(this EndpointFactoryConfigurator configurator)
        {
            UseTibco(configurator, x => { });
        }

        public static void UseTibco(this EndpointFactoryConfigurator configurator,
            Action<ITibcoTransportFactoryConfigurator> configureFactory)
        {
            var transportFactoryConfigurator = new TibcoTransportFactoryConfigurator();
            configureFactory(transportFactoryConfigurator);
            configurator.AddTransportFactory(transportFactoryConfigurator.Build());
            configurator.UseXmlSerializer();
        }
    }
}
