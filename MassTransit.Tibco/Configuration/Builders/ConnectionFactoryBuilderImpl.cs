﻿using System;
using System.Collections.Generic;
using Magnum.Extensions;
using TIBCO.EMS;

namespace MassTransit.Tibco.Configuration.Builders
{
    public class ConnectionFactoryBuilderImpl : IConnectionFactoryBuilder
    {
        private readonly ITibcoEndpointAddress _address;
        private readonly IList<Func<ConnectionFactory, ConnectionFactory>> _connectionFactoryConfigurators;

        public ConnectionFactoryBuilderImpl(ITibcoEndpointAddress address)
        {
            _address = address;
            _connectionFactoryConfigurators = new List<Func<ConnectionFactory, ConnectionFactory>>();
        }

        public ConnectionFactory Build()
        {
            ConnectionFactory connectionFactory = _address.ConnectionFactory;
            _connectionFactoryConfigurators.Each(x => x(connectionFactory));

            return connectionFactory;
        }

        public void Add(Func<ConnectionFactory, ConnectionFactory> callback)
        {
            _connectionFactoryConfigurators.Add(callback);
        }
    }
}
