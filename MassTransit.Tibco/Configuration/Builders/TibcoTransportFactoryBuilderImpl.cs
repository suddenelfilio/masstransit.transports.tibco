﻿using System;
using System.Collections.Generic;

namespace MassTransit.Tibco.Configuration.Builders
{
    public class TibcoTransportFactoryBuilderImpl : ITibcoTransportFactoryBuilder
    {
        readonly IDictionary<Uri, IConnectionFactoryBuilder> _connectionFactoryBuilders;

        public TibcoTransportFactoryBuilderImpl()
        {
            _connectionFactoryBuilders = new Dictionary<Uri, IConnectionFactoryBuilder>();
        }
        
        public void AddConnectionFactoryBuilder(Uri uri, IConnectionFactoryBuilder connectionFactoryBuilder)
        {
            _connectionFactoryBuilders[uri] = connectionFactoryBuilder;
        }

        public TibcoTransportFactory Build()
        {
            var factory = new TibcoTransportFactory(_connectionFactoryBuilders);
            return factory;
        }
    }
}