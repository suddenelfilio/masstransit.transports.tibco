﻿using System;

namespace MassTransit.Tibco.Configuration.Builders
{
    public interface ITibcoTransportFactoryBuilder
    {
        void AddConnectionFactoryBuilder(Uri uri, IConnectionFactoryBuilder connectionFactoryBuilder);
    }
}