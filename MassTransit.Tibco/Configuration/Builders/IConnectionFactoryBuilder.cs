using System;
using TIBCO.EMS;

namespace MassTransit.Tibco.Configuration.Builders
{
    public interface IConnectionFactoryBuilder
    {
        ConnectionFactory Build();

        void Add(Func<ConnectionFactory, ConnectionFactory> callback);
    }
}