﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit.Configurators;

namespace MassTransit.Tibco.Configuration.Configurators
{
    public class TibcoTransportFactoryConfiguratorImpl : ITibcoTransportFactoryConfigurator
    {
        readonly IList<ITibcoTransportFactoryBuilderConfigurator> _transportFactoryConfigurators;

        public TibcoTransportFactoryConfiguratorImpl()
        {
            _transportFactoryConfigurators = new List<ITibcoTransportFactoryBuilderConfigurator>();
        }

        public IEnumerable<ValidationResult> Validate()
        {
            throw _transportFactoryConfigurators.SelectMany(x => x.Validate());
        }

        public void AddConfigurator(ITibcoTransportFactoryBuilderConfigurator configurator)
        {
            _transportFactoryConfigurators.Add(configurator);
        }
    }
}
