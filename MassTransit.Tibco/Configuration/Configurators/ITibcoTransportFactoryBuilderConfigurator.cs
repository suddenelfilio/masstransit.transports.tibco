﻿using MassTransit.Configurators;
using MassTransit.Tibco.Configuration.Builders;

namespace MassTransit.Tibco.Configuration.Configurators
{
    public interface ITibcoTransportFactoryBuilderConfigurator : Configurator
    {
        ITibcoTransportFactoryBuilder Configure(ITibcoTransportFactoryBuilder builder);
    }
}