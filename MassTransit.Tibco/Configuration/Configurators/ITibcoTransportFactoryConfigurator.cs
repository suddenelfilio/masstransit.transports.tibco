﻿using MassTransit.Configurators;

namespace MassTransit.Tibco.Configuration.Configurators
{
    public interface ITibcoTransportFactoryConfigurator : Configurator
    {
        void AddConfigurator(ITibcoTransportFactoryBuilderConfigurator configurator);
    }
}