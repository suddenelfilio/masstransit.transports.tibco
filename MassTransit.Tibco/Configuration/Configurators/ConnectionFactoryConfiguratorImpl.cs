﻿using System.Collections.Generic;
using System.Linq;
using MassTransit.Configurators;
using MassTransit.Tibco.Configuration.Builders;

namespace MassTransit.Tibco.Configuration.Configurators
{
    public class ConnectionFactoryConfiguratorImpl : IConnectionFactoryConfigurator, ITibcoTransportFactoryBuilderConfigurator
    {

        readonly ITibcoEndpointAddress _address;

        readonly List<IConnectionFactoryBuilderConfigurator> _configurators;

        public ConnectionFactoryConfiguratorImpl(ITibcoEndpointAddress address)
        {
            _address = address;
            _configurators = new List<IConnectionFactoryBuilderConfigurator>();
        }

        public IEnumerable<ValidationResult> Validate()
        {
            return _configurators.SelectMany(x => x.Validate());
        }

        public ITibcoTransportFactoryBuilder Configure(ITibcoTransportFactoryBuilder builder)
        {
            IConnectionFactoryBuilder connectionFactoryBuilder = CreateBuilder();
            builder.AddConnectionFactoryBuilder(_address.Uri, connectionFactoryBuilder);
            return builder;
        }

        public IConnectionFactoryBuilder CreateBuilder()
        {
            var connectionFactoryBuilder = new ConnectionFactoryBuilderImpl(_address);
            _configurators.Aggregate((IConnectionFactoryBuilder)connectionFactoryBuilder,
                (seed, configurator) => configurator.Configure(seed));

            return connectionFactoryBuilder;
        }
    }
}
