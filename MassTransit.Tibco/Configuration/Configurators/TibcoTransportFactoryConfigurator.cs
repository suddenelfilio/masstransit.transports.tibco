using System.Collections.Generic;
using System.Linq;
using MassTransit.Configurators;
using MassTransit.Tibco.Configuration.Builders;

namespace MassTransit.Tibco.Configuration.Configurators
{
    public class TibcoTransportFactoryConfigurator : ITibcoTransportFactoryConfigurator
    {

        readonly IList<ITibcoTransportFactoryBuilderConfigurator> _transportFactoryConfigurators;

        public TibcoTransportFactoryConfigurator()
        {
            _transportFactoryConfigurators = new List<ITibcoTransportFactoryBuilderConfigurator>();
        }

        public IEnumerable<ValidationResult> Validate()
        {
            return _transportFactoryConfigurators.SelectMany(x => x.Validate());
        }

        public void AddConfigurator(ITibcoTransportFactoryBuilderConfigurator configurator)
        {
            _transportFactoryConfigurators.Add(configurator);
        }

        public TibcoTransportFactory Build()
        {
            var builder = new TibcoTransportFactoryBuilderImpl();
            _transportFactoryConfigurators.Aggregate((ITibcoTransportFactoryBuilder) builder,
                (seed, configurator) => configurator.Configure(seed));

            return builder.Build();
        }
    }
}