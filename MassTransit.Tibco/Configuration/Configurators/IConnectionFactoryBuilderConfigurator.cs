﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit.Configurators;
using MassTransit.Tibco.Configuration.Builders;

namespace MassTransit.Tibco.Configuration.Configurators
{
   public  interface IConnectionFactoryBuilderConfigurator:Configurator
   {
       IConnectionFactoryBuilder Configure(IConnectionFactoryBuilder builder);
   }
}
