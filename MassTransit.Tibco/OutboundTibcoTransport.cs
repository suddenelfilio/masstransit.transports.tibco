﻿using System;
using System.IO;
using MassTransit.Context;
using MassTransit.Transports;
using TIBCO.EMS;

namespace MassTransit.Tibco
{
    public class OutboundTibcoTransport : IOutboundTransport
    {
        private readonly ITibcoEndpointAddress _address;
        private readonly ConnectionHandler<TibcoConnection> _connectionHandler;
        private readonly SessionMode _mode;
        private readonly Object _lock = new object();
        private TibcoProducer _producer;

        public OutboundTibcoTransport(ITibcoEndpointAddress address,
            ConnectionHandler<TibcoConnection> connectionHandler,
            SessionMode mode)
        {
            _address = address;
            _connectionHandler = connectionHandler;
            _mode = mode;
        }

        public void Dispose()
        {
            RemoveProducer();
        }

        public IEndpointAddress Address
        {
            get { return _address; }
        }

        public void Send(ISendContext context)
        {
            AddProducerBinding();
            _connectionHandler.Use(connection =>
            {
                connection.Connect();
                using (var body = new MemoryStream())
                {
                    context.SerializeTo(body);
                    var msgid = context.MessageId ?? NewId.NextGuid().ToString();
                    _producer.Send(body, msgid);
                    _address.LogSent(msgid, context.MessageType);
                }
            });
        }

        private void AddProducerBinding()
        {
            if (_producer != null)
                return;

            lock (_lock)
            {
                if (_producer != null)
                    return;

                _producer = new TibcoProducer(_address, _mode);
                _connectionHandler.AddBinding(_producer);
            }
        }

        private void RemoveProducer()
        {
            if (_producer != null)
            {
                _connectionHandler.RemoveBinding(_producer);
            }
        }
    }
}