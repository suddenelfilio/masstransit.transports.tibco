﻿using TIBCO.EMS;

namespace MassTransit.Tibco
{
    public interface ITibcoEndpointAddress : IEndpointAddress
    {
        string Username { get; set; }
        string Password { get; set; }
        ConnectionFactory ConnectionFactory { get; }
    }
}