﻿using System;
using System.Threading;
using MassTransit.Util;
using TIBCO.EMS;

namespace MassTransit.Tibco
{
    public class TibcoEndpointAddress : ITibcoEndpointAddress
    {
        private static readonly string LocalMachineName = Environment.MachineName.ToLowerInvariant();
        private readonly Uri _uri;
        private readonly ConnectionFactory _connectionFactory;
        private Func<bool> _isLocal;

        public TibcoEndpointAddress(Uri uri, ConnectionFactory connectionFactory, string username = "", string password = "")
        {
            Username = username;
            Password = password;
            _uri = uri;
            _connectionFactory = connectionFactory;
            _isLocal = () => DetermineIfEndpointIsLocal(uri);
        }

        public Uri Uri
        {
            get { return _uri; }
        }

        public bool IsLocal
        {
            get { return _isLocal(); }
        }

        public bool IsTransactional
        {
            get { return false; }
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public ConnectionFactory ConnectionFactory
        {
            get { return _connectionFactory; }
        }

        public bool Equals(TibcoEndpointAddress other)
        {
            if (ReferenceEquals(null, other))
                return false;

            if (ReferenceEquals(this, other)) return true;
            return Equals(other._uri, _uri);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(TibcoEndpointAddress)) return false;
            return Equals((TibcoEndpointAddress)obj);
        }

        public override int GetHashCode()
        {
            return (_uri != null ? _uri.GetHashCode() : 0);
        }

        private bool DetermineIfEndpointIsLocal(Uri uri)
        {
            var hostName = uri.Host;
            var local = string.CompareOrdinal(hostName, ".") == 0 ||
                        string.Compare(hostName, "localhost", StringComparison.OrdinalIgnoreCase) == 0 ||
                        string.Compare(uri.Host, LocalMachineName, StringComparison.OrdinalIgnoreCase) == 0;

            Interlocked.Exchange(ref _isLocal, () => local);

            return local;
        }

        public static TibcoEndpointAddress Parse(Uri address)
        {
            if (address == null)
                throw new ArgumentNullException();

            if (string.Compare("tcp", address.Scheme, StringComparison.OrdinalIgnoreCase) != 0)
                throw new ArgumentException("Invalid scheme.");


            var cf = new ConnectionFactory(string.Format("{0}://{1}:{2}", address.Scheme, address.Host, address.Port), NewId.NextGuid().ToString());

            string username = address.Query.GetValueFromQueryString("user");
            string password = address.Query.GetValueFromQueryString("pwd");

            return new TibcoEndpointAddress(address, cf, username, password);
        }
    }
}