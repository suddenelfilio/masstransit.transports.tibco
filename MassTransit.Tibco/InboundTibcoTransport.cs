﻿using System;
using System.IO;
using System.Text;
using MassTransit.Context;
using MassTransit.Transports;
using TIBCO.EMS;

namespace MassTransit.Tibco
{
    public class InboundTibcoTransport : IInboundTransport
    {
        private readonly ITibcoEndpointAddress _address;
        private readonly ConnectionHandler<TibcoConnection> _connectionHandler;
        private readonly SessionMode _mode;
        private TibcoConsumer _consumer;
        private bool _disposed;

        public InboundTibcoTransport(ITibcoEndpointAddress address,
            ConnectionHandler<TibcoConnection> connectionHandler,
            SessionMode mode)
        {
            _address = address;
            _connectionHandler = connectionHandler;
            _mode = mode;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public IEndpointAddress Address
        {
            get { return _address; }
        }

        public void Receive(Func<IReceiveContext, Action<IReceiveContext>> lookupSinkChain, TimeSpan timeout)
        {
            AddConsumerBinding();
            _connectionHandler.Use(connection =>
            {
                connection.Connect();
                var msg = _consumer.Consumer.Receive(timeout.Seconds) as TextMessage;
                if (msg == null)
                    return;

                using (var body = new MemoryStream(Encoding.UTF8.GetBytes(msg.Text), false))
                {
                    var ctx = ReceiveContext.FromBodyStream(body);
                    ctx.SetMessageId(msg.MessageID);
                    ctx.SetInputAddress(_address);

                    var receive = lookupSinkChain(ctx);
                    if (receive == null)
                    {
                        Address.LogSkipped(msg.MessageID);
                    }
                    else
                    {
                        receive(ctx);
                        if (_mode == SessionMode.ClientAcknowledge ||
                            _mode == SessionMode.ExplicitClientAcknowledge ||
                            _mode == SessionMode.ExplicitClientDupsOkAcknowledge)
                            msg.Acknowledge();
                    }
                }
            });
        }

        private void AddConsumerBinding()
        {
            if (_consumer != null)
                return;

            _consumer = new TibcoConsumer(_address, _mode);

            _connectionHandler.AddBinding(_consumer);
        }

        private void RemoveConsumerBinding()
        {
            if (_consumer != null)
                _connectionHandler.RemoveBinding(_consumer);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                RemoveConsumerBinding();
            }

            _disposed = true;
        }
    }
}