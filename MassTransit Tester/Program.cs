﻿using System;
using MassTransit;
using MassTransit.Tibco.Configuration;

namespace MassTransit_Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Bus.Initialize(sbc =>
            {
                sbc.UseTibco(tibco => { });

                //assuming both server connection and queue/topic credentials are the same!
               sbc.ReceiveFrom("tcp://<someserver>:7222/<Destination-name>?user=<user>&pwd=<pwd>");
                sbc.Subscribe(subs =>
                {
                    subs.Handler<TestMessage>(msg => Console.WriteLine(msg.Text));
                });
            });
            Bus.Instance.Publish(new TestMessage {Text = "Hi"});
        }
    }

   [Serializable]
    public class TestMessage
    {
        public string Text { get; set; }
    }
}
